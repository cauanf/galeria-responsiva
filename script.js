const arrSize = ["181px","201.11px","221.22px","241.33px","261.44px","281.55px","301.66px","321.77px","341.88px","362px"];
const arrFotos = ["Thumbs Galeria/assets/Echos/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/TezaTalks/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/XYLO/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/AViVA/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/ALMA/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/Krewella/236x362 Clean/236x362.jpg","Thumbs Galeria/assets/Veela/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/K.Flay/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/Nevve/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/RIELL/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/Billie Eillish/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/Laura Brehm/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/Kerli/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/The Tech Thieves/236x362 Clean/236x362 Clean.jpg","Thumbs Galeria/assets/Jane XO/236x362 Clean/236x362 Clean.jpg"];
const arrLinks = ["http://www.youtube.com/watch?v=FdOifXvFq3U","https://www.youtube.com/watch?v=Em2JrUybWvM","https://www.youtube.com/watch?v=GtTxyw83Uxs","https://www.youtube.com/watch?v=abAfJ-sjW88","https://www.youtube.com/watch?v=-HFQs_2Uy1w","https://www.youtube.com/watch?v=gS4domW0_yQ","https://www.youtube.com/watch?v=v7BddpYYNGk","https://www.youtube.com/watch?v=jxJnIKSlZq4","https://www.youtube.com/watch?v=6QtNHoKl-eA","https://www.youtube.com/watch?v=BnQEVgKd-9Y","https://www.youtube.com/watch?v=-tn2S3kJlyU","https://www.youtube.com/watch?v=5eSTYgwCQqY","https://www.youtube.com/watch?v=GffHqfdzABU","https://www.youtube.com/watch?v=Nzf_sO8CzYg","https://www.youtube.com/watch?v=bLcNbXTAuAg"];

const galery = document.getElementById("galery");

const createHTML = (cleanImg, link) => {
    const divFotos = document.createElement("div");
    divFotos.className = "fotos";

    const href = document.createElement("a");
    href.href = link;
    href.target = "_blank";

    const imgs = document.createElement("img");
    imgs.className = "woOpacity";
    imgs.src = cleanImg;

    href.appendChild(imgs);
    divFotos.appendChild(href);
    galery.appendChild(divFotos);
};

for(let i = 0; i < arrLinks.length; i++){
    createHTML(arrFotos[i], arrLinks[i])
};

for(let i = 1; i <= arrLinks.length; i++){
    const piInUse = document.querySelector("#galery .fotos:nth-of-type("+i+")");
    piInUse.style.height = arrSize[Math.floor(Math.random()*(9 - 1))];
};